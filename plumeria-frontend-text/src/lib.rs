#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(pub parser);

use plumeria_syntax::Module;

pub fn parse(src: &str) -> Module {
    parser::ParseModuleParser::new().parse(src).unwrap()
}

#[cfg(test)]
mod tests {
    use crate::parse;
    use plumeria_syntax::Module;

    #[test]
    fn test_parse_mod() {
        let module = parse(
            r#"

        test() = 1337

        variable() = x

        closure_test() = (a())(b(c, d, 64))

        hole() = 0 _

        local() =
            let x = 6
            and y = 7
            in  plus(x, y)

        "#,
        );

        let json = serde_json::to_string_pretty(&module).unwrap();

        let read_again: Module = serde_json::from_str(&json).unwrap();

        assert_eq!(module, read_again);
    }
}
